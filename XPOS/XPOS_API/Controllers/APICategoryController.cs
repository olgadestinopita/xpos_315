﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using XPOS_API.Models;
using XPOS_viewModels;

namespace XPOS_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class APICategoryController : ControllerBase
    {
        //inisialisasi database untuk di controler ini aja
        private readonly XPOS_315Context db;

        private VMRespon respon = new VMRespon();
        public APICategoryController(XPOS_315Context _db)
        {
            this.db = _db;
        }
        //
        [HttpGet("GetAllCategory")]
        public List<TblCategory> GetAllCategory()
        {
            List<TblCategory> dataCategory = new List<TblCategory>();
            dataCategory = db.TblCategories.Where(a => a.IsDelete == false).ToList();
            return dataCategory;
        }

        [HttpGet("GetById/{id}")]
        public TblCategory GetById(int id)
        {
            TblCategory dataCategory = new TblCategory();
            dataCategory = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();
            return dataCategory;
        }
        [HttpPost("PostCategory")]
        public VMRespon PostCategory(TblCategory data)
        {
            data.IsDelete = false;
            data.CreateDate = DateTime.Now;
            try
            {
                db.Add(data);
                db.SaveChanges();
                respon.Message = "Data Success Saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "failed saved: " + e.Message;
            }

            return respon;
        }
        [HttpPut("PutCategory")]
        public VMRespon PutCategory(TblCategory data)
        {
            TblCategory dataOld=db.TblCategories.Where(a=>a.Id==data.Id).FirstOrDefault();
            if(dataOld!=null)
            {
                dataOld.NamaCategory = data.NamaCategory;
                dataOld.Description = data.Description;

                dataOld.UpdateDate = DateTime.Now;
                dataOld.UpdateBy = data.UpdateBy;
                try
                {


                    db.Update(dataOld);
                    db.SaveChanges();
                    respon.Message = "Data success updated";
                }catch(Exception e)
                {
                    respon.Success = false;
                    respon.Message="update faliled: " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "data not found: ";
            }
            return respon;
        }

        [HttpDelete("DeleteCategory/{id}")]
        public VMRespon DeleteCategory(int id)
        {
            TblCategory data=db.TblCategories.Where(a=>a.Id==id).FirstOrDefault(); 
            if(data!=null)
            {
                data.IsDelete = true;
                try
                {
                    db.Update(data);
                    db.SaveChanges();
                    respon.Message = "Data success deleted";
                }
                catch(Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data falied deleted: " + e.Message;
                }
            }
            else
            {
                respon.Success=false;
                respon.Message = "data not found";
            }
            return respon;
        }

       
    }
}
